// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}
// User
{
    "id": 4,
    "firstName": "Shendelzare",
    "lastName": "Silkwood",
    "email": "vengeance@mail.com",
    "password": "vsstun",
    "isAdmin": false,
    "mobileNo": "404"
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 11,
    "userId": 4,
    "productID" : 50,
    "transactionDate": "11-24-2022",
    "status": "paid",
    "total": 2000
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 50,
    "name": "Butterfly",
    "description": "Fly like a butterfly",
    "price": 2000,
    "stocks": 1,
    "isActive": true,
}


